// Andy Palmbach
// Lab Exercise 3

#include <iostream> // need for cout
#include <conio.h> // need for getch
#include <string>
#include <fstream>

using namespace std;

int main()
{
    //declaring variable
    string array[12];
    string story;

    // user entry
    cout << "Enter an adjective: ";
    getline(cin, array[0]);
    cout << "Enter a sport: ";
    getline(cin, array[1]);
    cout << "Enter a city name: ";
    getline(cin, array[2]);
    cout << "Enter a person: ";
    getline(cin, array[3]);
    cout << "Enter an action verb (past tense): ";
    getline(cin, array[4]);
    cout << "Enter a vehicle: ";
    getline(cin, array[5]);
    cout << "Enter a place: ";
    getline(cin, array[6]);
    cout << "Enter a noun (plural): ";
    getline(cin, array[7]);
    cout << "Enter an adjective: ";
    getline(cin, array[8]);
    cout << "Enter a food (plural): ";
    getline(cin, array[9]);
    cout << "Enter a liquid: ";
    getline(cin, array[10]);
    cout << "Enter an adjective: ";
    getline(cin, array[11]);

    //generating the story
    story = "\nOne day my " + array[0] + " friend and I decided to go to the " + array[1] + " game in " + array[2] + ".\n" 
        + "We really wanted to see " + array[3] + " play.\n" 
        + "So we " + array[4] + " in the " + array[5] + " and headed down to " + array[6] + " and bought some " + array[7] + ".\n" 
        + "We watched the game and it was " + array[8] + ".\n" + "We ate some " + array[9] + " and drank some " + array[10] + ".\n" 
        + "We had a " + array[11] + " time, and can't wait to go again.\n";

    //printing story to console
    cout << story;

    //asking if user wants to save story
    char input;
    cout << "\nWould you like to save your madlib to a text file? Enter y for yes or n for no. ";
    cin >> input;

    if (input == 'y')
    {
        //sending story to text file
        string path = "C:\\SoftwareDev\\madlib.txt";
        ofstream ofs(path);
        ofs << story;
        ofs.close();
        cout << "\nMadlib saved at C:\\SoftwareDev\\madlib.text. You may now exit the program. ";
    }
    else
    {
        return 0;
    }

    /*
    cout << "One day my " << array[1] << " friend and I decided to go to the " << array[2] << " game in " << array[3] << ".";
    cout << "We really wanted to see " << array[4] << " play.";
    cout << "So we " << array[5] << " in the " << array[6] << " and headed down to " << array[7] << " and bought some " << array[8] << ".";
    cout << "We watched the game and it was " << array[9] << ".";
    cout << "We ate some " << array[10] << " and drank some " << array[11] << ".";
    cout << "We had a " << array[12] << " time, and can't wait to go again.";
    */

    _getch();
    return 0;
}